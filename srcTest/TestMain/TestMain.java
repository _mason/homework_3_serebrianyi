package TestMain;

import Main.Main;
import org.junit.Assert;
import org.junit.Test;

public class TestMain {

    @Test
    public void TestMinOfThre(){
        Main main = new Main();
        int result = Main.minOfThree(1,4,2);

        Assert.assertEquals(1,result);
    }

    @Test
    public void Testfactorial(){
        Main main = new Main();
        int result = Main.factorial(4);

        Assert.assertEquals(24,result);
    }

    @Test
    public void TestcompareString(){
        Main main = new Main();
        Assert.assertTrue(Main.compareString(new String("abc"), new String("abc")));

//        Boolean result = Main.compareString("abc","abc");
//       Assert.assertEquals(false,result);
    }

    @Test
    public void TestconcatenateString(){
        Main main = new Main();
        String result = Main.concatenateString("asleep, ","sleep");

        Assert.assertEquals("asleep, sleep",result);
    }

    @Test
    public void TestfindMinInArray(){
        Main main = new Main();
        int result = Main.findMinInArray(new int[]{1, 3, 5, 2, 5, 2, 4, 0});

        Assert.assertEquals(0,result);
    }

    @Test
    public void TestsortArray(){
        Main main = new Main();
        int[] result = Main.sortArray(new int[]{4,6,1,5});

        Assert.assertEquals(new int[]{6,5,4,1},result);
    }

    @Test
    public void TestsumOfElementsUnderMainDiagonal(){
        Main main = new Main();
        int result = main.sumOfElementsUnderMainDiagonal(new int[][]{{4,2,6,8},{5,1,4,2},{5,2,5,2},{5,2,5,6}});

        Assert.assertEquals(24,result);
    }

    @Test
    public void TestcountPowerOfTwo(){
        Main main = new Main();
        int result = main.countPowerOfTwo(new int[][]{{7,1,3,0},{4,0,2,1},{5,3,2,5},{4,6,0,3}});

        Assert.assertEquals(5,result);
    }

    @Test(timeout=1000)
    public void TestsleepFor1000(){

    }

    @Test(expected=Exception.class)
    public void TestthrowException(){

    }
}
